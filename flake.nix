{
  description = "Docker Images for Nix Flakes";

  inputs = {
    utils.url = "github:numtide/flake-utils";
    docker-nixpkgs = {
      url = "github:nix-community/docker-nixpkgs";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, docker-nixpkgs }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}".extend (
        import "${docker-nixpkgs}/overlay.nix"
      );
    in rec {
      # `nix build`
      packages.image = pkgs.dockerTools.buildImage {
        name    = "jd91mzm2/nix-flakes";
        tag     = "latest";
        created = "now";

        fromImage = pkgs.docker-nixpkgs.nix-unstable;
        fromImageName = "nixpkgs/nix-unstable";
        fromImageTag  = "latest";

        runAsRoot = ''
          #!${pkgs.runtimeShell}
          mkdir -p /etc/nix
          echo "experimental-features = nix-command flakes ca-references" >> /etc/nix/nix.conf
          echo "sandbox = false" >> /etc/nix/nix.conf
        '';

        contents = with pkgs; [
          utillinux # For `mount --bind` caching
        ];

        config = {
          Cmd = [ "/bin/bash" ];
          Env = [
            "PATH=/usr/bin:/bin:/root/.nix-profile/bin"
          ];
        };
      };
      defaultPackage = packages.image;
    });
}
