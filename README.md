# Docker Image with Nix Flakes

Flakes is an experimental Nix feature that makes communicating between Nix
different repositories a lot more convenient. But I haven't found an easy way
to use flake powers in the GitLab CI. This small container image is based on
[docker-nixpkgs](https://gitlab.com/zimbatm/docker-nixpkgs), which has
Nix-built images for different Nix versions. I simply did some final polishing
on the nix-unstable image.

## Usage

This is how a simple job could look in your `.gitlab-ci.yml`.

``` yaml
Build:
  image: jd91mzm2/nix-flakes
  stage: build
  script:
    - nix flake test .
    - nix build .
```

If you need caching, you can hack around the nix store being at `/nix` to let
GitLab cache a local `.nix` directory, using a simple bind mount.

Add this to your build:

``` yaml
  before_script:
    # GitLab can't cache /nix directly
    - mkdir -p .nix
    - mount --bind /nix .nix
  cache:
    paths:
      - .nix
```

## Example

See this repo's `.gitlab-ci.yml` :)
