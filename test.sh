#!/usr/bin/env bash

set -euo pipefail

CONTAINER=
if type podman &> /dev/null; then
  CONTAINER=podman
elif type docker &> /dev/null; then
  CONTAINER=docker
fi

nix build .
"$CONTAINER" load < result
"$CONTAINER" run -it localhost/jd91mzm2/nix-flakes:latest
